#!/bin/bash

#
#    Copyright 2022 The DSMS Authors.
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

# Find the latest JAR file in the directory
JAR_FILE=$(ls -t /etc/dsms/dsms-engine/*.jar | head -1)

# Get the Java version of the current server
java_version=$(java -version 2>&1 | awk -F '"' '/version/ {print $2}' | awk -F. '{print $1}')

java_path=$(which java)

# Check if Java version is less than 11
if [[ "$java_version" -lt "11" ]]; then
  java_path=$(which java-11-openjdk)
  if [[ -z "$java_path" ]]; then
    java_11_path=$(find /usr/lib/jvm -type d -name "java-11-openjdk*" | head -n 1)
    java_path=${java_11_path}/bin/java
    if [[ -z "$java_path" ]]; then
      echo "dsms-engine needs java 11 or higher"
      exit 1
    fi
  fi
fi


${java_path} -jar "$JAR_FILE"
