%define _topdir @RPM_BUILD_DIR@
%define name @RPM_NAME@
%define version @RPM_VERSION@
%define release @RPM_RELEASE@
%define arch @RPM_ARCH@

Name:           %{name}
Version:        %{version}
Release:        %{release}
Summary:        DSMS ENGINE components, providing a restful api.
License:        MIT
BuildArch:      %{arch}

Requires: redis,mariadb-server,java-11

%description
ENGINE components, providing a restful api for user management and use of DSMS systems.
%install
mkdir -p %{buildroot}/etc/dsms/dsms-engine/
mkdir -p %{buildroot}/etc/dsms/dsms-engine/sql/
mkdir -p %{buildroot}/etc/systemd/system/
cp -r dsms-engine/* %{buildroot}/etc/dsms/dsms-engine/
cp dsms-engine/dsms-engine.service %{buildroot}/etc/systemd/system

%post

systemctl start mariadb
systemctl enable mariadb

mysql -u root <<EOF
ALTER USER 'root'@'localhost' IDENTIFIED BY 'root';
CREATE DATABASE dsms_engine;
use dsms_engine;
source /etc/dsms/dsms-engine/dsms-engine_init.sql
EOF

systemctl start redis
systemctl enable redis

%posttrans
mkdir -p /var/log/dsms/dsms-engine
systemctl start dsms-engine
systemctl enable dsms-engine

%preun
systemctl stop dsms-engine
rm -rf /var/lib/mysql/
rm -rf /etc/my.cnf

%postun


%files
/etc/*

%changelog