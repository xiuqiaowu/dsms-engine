/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.dsms.common.model;

import com.dsms.common.constant.ResultCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

/**
 * Standard response body
 */
@Data
@ApiModel(value = "接口返回对象", description = "接口返回对象")
public class Result<T> implements Serializable {

    /**
     * return HTTP status code
     */
    @ApiModelProperty(value = "HTTP标准码")
    private Integer status = 0;

    /**
     * return service status code
     */
    @ApiModelProperty(value = "业务状态码")
    private Integer code = 0;

    /**
     * return message
     */
    @ApiModelProperty(value = "返回消息")
    private String message = "";


    /**
     * return data
     */
    @ApiModelProperty(value = "返回数据")
    private T data;

    /**
     * return current timestamp
     */
    @ApiModelProperty(value = "时间戳")
    private long timestamp = System.currentTimeMillis();

    /**
     * ok result
     *
     * @return response body
     */
    public static <T> Result<T> OK() {
        Result<T> r = new Result<>();

        r.setStatus(ResultCode.OK.getCode());
        r.setCode(ResultCode.SUCCESS.getCode());
        r.setMessage(ResultCode.SUCCESS.getMessage());

        return r;
    }

    /**
     * ok result with data
     *
     * @param data data
     * @return response body
     */
    public static <T> Result<T> OK(T data) {
        Result<T> r = new Result<>();

        r.setStatus(ResultCode.OK.getCode());
        r.setCode(ResultCode.SUCCESS.getCode());
        r.setMessage(ResultCode.SUCCESS.getMessage());
        r.setData(data);

        return r;
    }

    /**
     * ok result with resultCode
     *
     * @param resultCode dsms standard status code
     * @return response body
     */
    public static <T> Result<T> OK(ResultCode resultCode) {
        Result<T> r = new Result<>();

        r.setStatus(ResultCode.OK.getCode());
        r.setCode(resultCode.getCode());
        r.setMessage(resultCode.getMessage());

        return r;
    }

    /**
     * error result with resultCode
     *
     * @param resultCode dsms standard status code
     * @return response body
     */
    public static <T> Result<T> error(ResultCode resultCode) {
        Result<T> r = new Result<>();

        r.setStatus(ResultCode.SERVER_ERROR.getCode());
        r.setCode(resultCode.getCode());
        r.setMessage(resultCode.getMessage());

        return r;
    }

    /**
     * error with errorMessage
     *
     * @param errorMessage exception message
     * @return response body
     */
    public static <T> Result<T> error(String errorMessage) {
        Result<T> r = new Result<>();

        r.setStatus(ResultCode.SERVER_ERROR.getCode());
        r.setCode(ResultCode.SERVER_ERROR.getCode());
        r.setMessage(errorMessage);

        return r;
    }

    /**
     * The HTTP status code is returned
     *
     * @param resultCode HTTP status code
     * @return response body
     */
    public static <T> Result<T> httpRes(ResultCode resultCode) {
        Result<T> r = new Result<>();

        r.setStatus(resultCode.getCode());
        r.setCode(resultCode.getCode());
        r.setMessage(resultCode.getMessage());

        return r;
    }

    /**
     * normal result，HTTP+service code
     *
     * @param status HTTP status code
     * @param code   service status code
     * @return response body
     */
    public static <T> Result<T> normal(HttpStatus status, ResultCode code) {
        Result<T> r = new Result<>();

        r.setStatus(status.value());
        r.setCode(code.getCode());
        r.setMessage(code.getMessage());

        return r;
    }

    /**
     * normal result
     *
     * @param httpStatus HTTP status code
     * @param code       service status code
     * @param message    detail message
     * @param data       data
     * @return response body
     */
    public static <T> Result<T> normal(int httpStatus, int code, String message, T data) {
        Result<T> r = new Result<>();

        r.setStatus(httpStatus);
        r.setCode(code);
        r.setMessage(message);
        r.setData(data);

        return r;
    }


}
