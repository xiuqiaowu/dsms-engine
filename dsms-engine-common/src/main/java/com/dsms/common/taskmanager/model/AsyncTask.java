/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.dsms.common.taskmanager.model;

import lombok.NoArgsConstructor;

/**
 * Asynchronous task class
 */
@NoArgsConstructor
public class AsyncTask extends Task {

    public AsyncTask(String taskName, String taskType, Integer taskStatus, String taskMessage, String taskParam) {
        super(taskName, taskType, taskStatus, taskMessage, taskParam);
    }

    public AsyncTask(String taskName, String taskType, Integer taskStatus, String taskMessage, String taskParam, int isExclusive) {
        super(taskName, taskType, taskStatus, taskMessage, taskParam, isExclusive);
    }
}
