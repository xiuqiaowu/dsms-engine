/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.dsms.common.remotecall.model;


import lombok.Data;
import org.springframework.http.HttpMethod;

/**
 * dsms-storage request
 */
@Data
public class RemoteRequest {
    /**
     * dsms-storage request type
     */
    private String remoteType;
    /**
     * dsms-storage request method
     */
    private HttpMethod httpMethod;
    /**
     * dsms-storage request url prefix
     */
    private String urlPrefix;
    /**
     * dsms-storage request body
     */
    private String requestBody;

    private RemoteFixedParam remoteFixedParam;

    public RemoteRequest buildRequestParam(String urlPrefix, HttpMethod httpMethod, String requestBody) {
        this.urlPrefix = urlPrefix;
        this.httpMethod = httpMethod;
        this.requestBody = requestBody;
        return this;
    }
    public RemoteRequest buildRequestParam(RemoteRequest remoteRequest) {
        this.urlPrefix = remoteRequest.urlPrefix;
        this.httpMethod = remoteRequest.httpMethod;
        this.requestBody = remoteRequest.requestBody;
        return this;
    }

    public RemoteRequest() {
    }

    public RemoteRequest(RemoteFixedParam remoteFixedParam) {
        this.remoteFixedParam = remoteFixedParam;
    }
}
