/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.dsms.common.remotecall.model;


import com.alibaba.fastjson2.annotation.JSONField;

import java.util.Arrays;
import java.util.List;

/**
 * dsms-storage response
 */
//@Data
public class RemoteResponse {
    /**
     * response body
     */
    private String body;
    /**
     * response message
     */
    private String message;
    /**
     * response failed field
     */
    private List<FailedDetail> failed;

    /**
     * response finished field
     */
    private List<FinishedDetail> finished;
    /**
     * response has_failed field
     */
    @JSONField(name = "has_failed")
    private boolean hasFailed;
    /**
     * response id field
     */
    private String id;
    /**
     * response is_finished field
     */
    @JSONField(name = "is_finished")
    private boolean isFinished;
    /**
     * response is_waiting field
     */
    @JSONField(name = "is_waiting")
    private boolean isWaiting;
    /**
     * response running field
     */
    private String[] running;
    /**
     * response state field
     */
    private String state;
    /**
     * response waiting field
     */
    private String[] waiting;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<FailedDetail> getFailed() {
        return failed;
    }

    public void setFailed(List<FailedDetail> failed) {
        this.failed = failed;
    }


    public boolean isHasFailed() {
        return hasFailed;
    }

    public void setHasFailed(boolean hasFailed) {
        this.hasFailed = hasFailed;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean getIsFinished() {
        return isFinished;
    }

    public void setIsFinished(boolean isFinished) {
        this.isFinished = isFinished;
    }

    public boolean getIsWaiting() {
        return isWaiting;
    }

    public void setIsWaiting(boolean isWaiting) {
        this.isWaiting = isWaiting;
    }

    public String[] getRunning() {
        return running;
    }

    public void setRunning(String[] running) {
        this.running = running;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String[] getWaiting() {
        return waiting;
    }

    public void setWaiting(String[] waiting) {
        this.waiting = waiting;
    }

    public List<FinishedDetail> getFinished() {
        return finished;
    }

    public void setFinished(List<FinishedDetail> finished) {
        this.finished = finished;
    }

    @Override
    public String toString() {
        return "RemoteResponse{" +
                "body='" + body + '\'' +
                ", message='" + message + '\'' +
                ", failed='" + failed + '\'' +
                ", finished=" + finished +
                ", hasFailed=" + hasFailed +
                ", id='" + id + '\'' +
                ", isFinished=" + isFinished +
                ", isWaiting=" + isWaiting +
                ", running=" + Arrays.toString(running) +
                ", state='" + state + '\'' +
                ", waiting=" + Arrays.toString(waiting) +
                '}';
    }
}
