/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.dsms.common.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * the crush failure domain in crushmap
 */
@Getter
@AllArgsConstructor
public enum CrushFailureDomainEnum {

    HOST(1, "host"),
    ROOT(11, "root");

    //use crushmap's origin define
    private final int typeId;
    private final String type;

    public static String getCrushFailureDomain(int typeId) {
        CrushFailureDomainEnum[] values = CrushFailureDomainEnum.values();
        for (CrushFailureDomainEnum crushFailureDomainEnum : values) {
            if (crushFailureDomainEnum.getTypeId() == typeId) {
                return crushFailureDomainEnum.getType();
            }
        }
        throw new IllegalArgumentException("unknown crush failure domain");
    }

}
