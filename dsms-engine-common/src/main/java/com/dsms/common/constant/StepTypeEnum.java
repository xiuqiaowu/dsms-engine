/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.common.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * task type enum
 */
@Getter
@AllArgsConstructor
public enum StepTypeEnum {
    /**
     * create a osd
     */
    CREATE_OSD("create_osd", "创建osd"),
    /**
     * move osd to its default location in crushmap
     */
    MOVE_OSD_TO_DEFAULT("move_osd", "移动osd至默认位置"),
    /**
     * stop the osd service
     */
    STOP_OSD("stop_osd", "停止osd服务"),
    /**
     * purge the osd
     */
    PURGE_OSD("purge_osd", "将osd移除节点"),
    /**
     * zap the osd
     */
    ZAP_OSD("zap_osd", "擦除osd磁盘"),
    /**
     * create a root bucket in crushmap
     */
    CREATE_ROOT_BUCKET("create_root_bucket", "创建root级bucket"),
    /**
     * create a host bucket in crushmap
     */
    CREATE_HOST_BUCKET("create_host_bucket", "创建host级bucket"),
    /**
     * delete a host bucket in crushmap
     */
    DELETE_HOST_BUCKET("delete_host_bucket", "删除host级bucket"),
    /**
     * create a rule in crushmap
     */
    CREATE_RULE("create_rule", "创建副本池rule"),
    /**
     * create a ec-profile in crushmap
     */
    CREATE_ECPROFILE("create_ec-profile", "创建纠删码配置文件"),
    /**
     * create a replicated storage pool
     */
    CREATE_REPLICATED_POOL("create_replicated_pool", "创建副本池"),
    /**
     * create a erasure storage pool
     */
    CREATE_ERASURE_POOL("create_erasure_pool", "创建纠删码池"),
    /*
     * allow erasure pool can create rbd and filesystem
     * */
    ALLOW_EC_OVER_WRITES("allow_ec_over_writes", "允许纠删码池覆写"),
    /**
     * storage pool add disk
     */
    POOL_ADD_DISK("pool_add_disk", "存储池添加磁盘"),
    /**
     * storage pool remove disk
     */
    POOL_REMOVE_DISK("pool_remove_disk", "存储池移除磁盘"),
    /**
     * delete storage pool
     */
    DELETE_POOL("delete_pool", "删除存储池"),
    /**
     * delete rule in crushmap
     */
    DELETE_RULE("delete_rule", "删除存储池rule"),
    /**
     * return osd to it's default node
     */
    RETURN_OSD_TO_NODE("return_osd_to_node", "将osd移动回默认节点"),
    /**
     * delete bucket in crushmap
     */
    DELETE_POOL_BUCKET("delete_pool_bucket", "删除存储池所有bucket"),
    /**
     * delete ec-profile in crushmap
     */
    DELETE_ECPROFILE("delete_ec-profile", "删除纠删码配置文件"),
    CREATE_STORAGE_DIR("create_storage_dir", "创建存储目录"),
    AUTH_STORAGE_DIR("auth_storage_dir", "授权存储目录"),
    FAIL_FILE_SYSTEM("fail_file_system", "关闭文件系统"),
    DELETE_FILE_SYSTEM("delete_file_system", "删除文件系统"),
    PURGE_FILE_SYSTEM_POOL("purge_file_system_pool", "清理文件系统存储池"),
    DELETE_FILE_SYSTEM_AUTH("delete_file_system_auth", "清理文件系统认证id"),
    DELETE_STORAGE_DIR("delete_storage_dir", "删除存储目录"),
    DEAUTH_STORAGE_DIR("deauth_storage_dir", "取消授权存储目录");
    private final String type;
    private final String name;

}
