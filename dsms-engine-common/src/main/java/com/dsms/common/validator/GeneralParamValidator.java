/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.common.validator;

import com.dsms.common.constant.NameSchemeEnum;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.stream.Stream;

public class GeneralParamValidator implements ConstraintValidator<GeneralParam, String> {

    private NameSchemeEnum nameSchemeEnum;

    @Override
    public void initialize(GeneralParam constraintAnnotation) {
        nameSchemeEnum = constraintAnnotation.nameSchemeEnum();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        String regex = nameSchemeEnum.getRegex();
        boolean isMatch = Stream.of(value).anyMatch(name -> name.matches(regex));
        if (!isMatch) {
            String message = nameSchemeEnum.getResourceName() + nameSchemeEnum.getMessage();
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(message).addConstraintViolation();
            return false;
        }
        return true;
    }
}
