/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.task.service;

import com.dsms.common.taskmanager.model.Step;
import com.dsms.common.taskmanager.service.IStepService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StepServiceTest {
    @Autowired
    private IStepService stepService;


    @Test
    @DisplayName("子任务模块crud")
    void testNormal() throws Exception {
        // mock a step bean
        Step step = new Step(null, 1, "test", "create", 1, "子任务描述", "1111", LocalDateTime.MIN, LocalDateTime.MIN, null, null);
        // assert create feature
        boolean result = stepService.save(step);
        assertTrue(result);
        Step stepEntity = stepService.getById(step.getId());
        assertNotNull(stepEntity);
        // assert update feature
        stepEntity.setStepName("new_name");
        boolean result1 = stepService.updateById(stepEntity);
        assertTrue(result1);
        assertEquals(step.getId(), stepEntity.getId());
        Step stepNew = stepService.getById(stepEntity.getId());
        assertEquals(stepNew.getStepName(), stepEntity.getStepName());
        // assert delete feature
        stepService.removeById(stepEntity.getId());
        Step stepToDelete = stepService.getById(stepEntity.getId());
        assertNull(stepToDelete);
    }

}