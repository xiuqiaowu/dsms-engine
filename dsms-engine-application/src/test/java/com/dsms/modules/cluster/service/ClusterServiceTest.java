/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.cluster.service;

import com.dsms.common.constant.ResultCode;
import com.dsms.common.exception.DsmsEngineException;
import com.dsms.common.model.LoginUser;
import com.dsms.common.remotecall.service.IRemoteCall;
import com.dsms.dfsbroker.cluster.api.ClusterApi;
import com.dsms.dfsbroker.cluster.model.Cluster;
import com.dsms.dfsbroker.cluster.service.IClusterService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ClusterServiceTest {

    @LocalServerPort
    int port;

    @Autowired
    private IClusterService clusterService;

    @MockBean
    private ClusterApi clusterApi;
    @MockBean
    private IRemoteCall remoteCall;

    @BeforeAll
    public static void initUser() {
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(new LoginUser(1, "admin", "password"), null));
    }

    @BeforeEach
    public void mockDsmsStorageResult() {
        //Mock dsms-storage cluster api result
        Mockito.when(clusterApi.getClusterVersion(any()))
                .thenReturn(new Cluster());
    }

    @Test
    @DisplayName("集群模块crud")
    @Transactional
    void testNormal() throws Exception {
        // mock a cluster bean
        Cluster cluster = new Cluster();
        cluster.setClusterName("test");
        cluster.setClusterAddress("127.0.0.1");
        cluster.setClusterPort(8003);
        cluster.setAuthKey("authkey");
        // assert create feature
        boolean result = clusterService.save(cluster);
        assertTrue(result);
        Cluster clusterEntity = clusterService.getById(cluster.getId());
        assertNotNull(clusterEntity);
        // assert update feature
        clusterEntity.setClusterName("new_name");
        boolean result1 = clusterService.updateById(clusterEntity);
        assertTrue(result1);
        assertEquals(cluster.getId(), clusterEntity.getId());
        Cluster clusterNew = clusterService.getById(clusterEntity.getId());
        assertEquals(clusterNew.getClusterName(), clusterEntity.getClusterName());
        // assert delete feature
        clusterService.removeById(clusterEntity.getId());
        Cluster clusterToDelete = clusterService.getById(clusterEntity.getId());
        assertNull(clusterToDelete);
    }

    @Test
    @DisplayName("集群模块-绑定功能测试")
    void testBind() {
        Cluster cluster = new Cluster();
        cluster.setClusterAddress("127.0.0.1");
        cluster.setClusterName("dev");
        cluster.setAuthKey("authKey");
        cluster.setClusterPort(port);
        Cluster bind = clusterService.bind(cluster);
        Cluster about = clusterService.about();
        assertEquals(bind.getId(), about.getId());
        boolean unbind = clusterService.unbind();
        assertTrue(unbind);
        assertEquals(ResultCode.CLUSTER_NOTBIND, assertThrows(DsmsEngineException.class, () -> {
            clusterService.about();
        }).getCode());
    }


    @Test
    @DisplayName("集群模块-异常测试")
    void testBindException() {

        Cluster cluster = new Cluster();
        cluster.setClusterAddress("256.256.256.256");
        assertEquals(ResultCode.CLUSTER_ADDRESS_NET_ERROR, assertThrows(DsmsEngineException.class, () -> {
            clusterService.bind(cluster);
        }).getCode());
        cluster.setClusterAddress("127.0.0.1");
        cluster.setClusterPort(65535);
        assertEquals(ResultCode.CLUSTER_SERVICEERROR, assertThrows(DsmsEngineException.class, () -> {
            clusterService.bind(cluster);
        }).getCode());

        cluster.setClusterPort(port);
        given(clusterApi.getClusterVersion(any())).willThrow(new AuthenticationServiceException("auth: Incorrect password"));
        assertEquals(ResultCode.CLUSTER_KEY_ERROR, assertThrows(DsmsEngineException.class, () -> {
            clusterService.bind(cluster);
        }).getCode());
        //中问
    }


}