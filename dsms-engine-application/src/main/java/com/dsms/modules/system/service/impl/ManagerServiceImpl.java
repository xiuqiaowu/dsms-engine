/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.system.service.impl;

import cn.hutool.core.io.resource.ResourceUtil;
import com.dsms.dfsbroker.cluster.api.ClusterApi;
import com.dsms.dfsbroker.cluster.model.remote.ServerResult;
import com.dsms.modules.system.model.vo.ComponentVO;
import com.dsms.modules.system.service.IManagerService;
import com.dsms.modules.util.RemoteCallUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.GitProperties;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;

import static com.dsms.common.constant.SystemConst.RELEASE_VERSION_FILE_PATH;
import static com.dsms.common.constant.SystemConst.VERSION_FILE_PATH;


@Slf4j
@Service
public class ManagerServiceImpl implements IManagerService {

    @Autowired
    private GitProperties gitProperties;

    @Autowired
    private ClusterApi clusterApi;

    @Override
    public ComponentVO getComponentAbout() {
        ComponentVO componentVO = new ComponentVO();
        //1. get git info
        componentVO.setDsmsEngineCommitId(gitProperties.getCommitId());
        componentVO.setDsmsEngineCommitTags(gitProperties.get("tags"));
        //2. get dsms-engine version
        String version = "unknown";
        try {
            version = ResourceUtil.readStr(VERSION_FILE_PATH, StandardCharsets.UTF_8);
        } catch (RuntimeException e) {
            log.error("get dsms-engine version fail,fail message:{}", e.getMessage(), e);
        }
        componentVO.setDsmsEngineVersion(version);
        //3. get release version
        String releaseVersion = "unknown";
        try {
            File releaseVersionFile = ResourceUtils.getFile(RELEASE_VERSION_FILE_PATH);
            if (releaseVersionFile.exists()) {
                releaseVersion = Files.readString(releaseVersionFile.toPath(), StandardCharsets.UTF_8);
            }
        } catch (IOException e) {
            log.error("get dsms release version fail,fail message:{}", e.getMessage(), e);
        }
        componentVO.setDsmsReleaseVersion(releaseVersion);

        //4. get dsms-storage  version,if get fail show null
        List<ComponentVO.StorageVersion> storageVersions = null;
        try {
            List<ServerResult> server = clusterApi.getServer(RemoteCallUtil.generateRemoteRequest());
            storageVersions = ComponentVO.parseServiceToStorageVersions(server);
        } catch (Throwable e) {
            log.error("get dsms-storage version fail,fail message:{}", e.getMessage(), e);
        }
        componentVO.setStorageVersions(storageVersions);

        return componentVO;
    }
}
