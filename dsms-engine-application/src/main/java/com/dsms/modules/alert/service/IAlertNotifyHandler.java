/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.alert.service;

import com.dsms.modules.alert.entity.AlertApprise;
import com.dsms.modules.alert.entity.AlertMessage;

public interface IAlertNotifyHandler {

    /**
     * Get the message send type.
     *
     * @return message type
     */
    Integer getType();


    /**
     * Send alert message
     */
    void sendAlertMessage(AlertApprise apprise, String alertMessage);

    /**
     * Update alertMessageApprise status(smsStatus/emailStatus)
     */
    void updateAlertMessageAppriseStatus(AlertMessage alertMessage, boolean isSuccess);

}
