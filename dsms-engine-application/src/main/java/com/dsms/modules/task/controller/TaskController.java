/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.task.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dsms.common.model.Result;
import com.dsms.common.taskmanager.model.Task;
import com.dsms.common.taskmanager.service.ITaskService;
import com.dsms.modules.task.model.TaskPageVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Task controller
 */
@RestController
@RequestMapping("/api/task")
@Api(tags = "任务模块")
public class TaskController {

    @Autowired
    private ITaskService taskService;

    @ApiOperation("任务模块-获取任务列表")
    @PostMapping("/list")
    public Result<Page<Task>> list(@Validated @RequestBody TaskPageVO taskPageVO) {
        Page<Task> taskPage = new Page<>(taskPageVO.getPageNo(), taskPageVO.getPageSize());
        LambdaQueryWrapper<Task> queryWrapper = new LambdaQueryWrapper<>();
        if (!ObjectUtils.isEmpty(taskPageVO.getTaskStatus())) {
            queryWrapper.in(Task::getTaskStatus, taskPageVO.getTaskStatus());
        }
        queryWrapper.orderByDesc(Task::getTaskStartTime);
        taskPage = taskService.page(taskPage, queryWrapper);

        return Result.OK(taskPage);
    }

}
