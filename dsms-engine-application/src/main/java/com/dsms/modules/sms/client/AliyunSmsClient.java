/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.sms.client;


import com.alibaba.fastjson2.JSON;
import com.aliyun.dysmsapi20170525.Client;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.teaopenapi.models.Config;
import com.aliyun.teautil.models.RuntimeOptions;
import com.dsms.common.constant.CommonEnum;
import com.dsms.modules.sms.AbstractSmsClient;
import com.dsms.modules.sms.config.SmsProviderProperties;
import java.net.URL;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;

/**
 * Aliyun sms client
 */
@Slf4j
public class AliyunSmsClient extends AbstractSmsClient {

    /**
     * Aliyun sms client
     */
    private Client client;
    private RuntimeOptions runtime = new RuntimeOptions();

    private static final String RESPONSE_OK = "OK";


    public AliyunSmsClient(SmsProviderProperties properties) {
        super(properties);
    }

    @Override
    protected void doInit(){
        Config config = new Config().setAccessKeyId(properties.getSmsAccessId()).setAccessKeySecret(properties.getSmsAccessSecret());
        try {
            if (properties.isProxyEnabled()) {
                URL url = new URL(properties.getProxyScheme(), properties.getProxyHost(), properties.getProxyPort(), "/");
                runtime.setHttpProxy(url.toString());
            }
            client = new Client(config);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
     }

    @Override
    protected void doSendSms(String uuid, String[] mobiles, String signName, String apiTemplateId, List<Map<String, Object>> templateParams) {
        SendSmsRequest request = new SendSmsRequest();
        request.setPhoneNumbers(String.join(",", mobiles));
        request.setSignName(signName);
        request.setTemplateCode(apiTemplateId);
        request.setTemplateParam(JSON.toJSONString(templateParams));
        request.setOutId(uuid);

        SendSmsResponse sendSmsResponse = null;
        try {
            sendSmsResponse = client.sendSmsWithOptions(request, runtime);
            if (!RESPONSE_OK.equals(sendSmsResponse.getBody().getCode())) {
                throw new RuntimeException(sendSmsResponse.getBody().getMessage());
            }
        } catch (Exception e) {
            log.error("Aliyun sms send error. error message:{},request:{},response:{}", e.getMessage(), JSON.toJSONString(request), JSON.toJSONString(sendSmsResponse), e);
            throw new RuntimeException(e.getMessage(), e);
        }
    }

}
