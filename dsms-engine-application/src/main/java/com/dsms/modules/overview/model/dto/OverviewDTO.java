/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.overview.model.dto;

import com.dsms.modules.monitor.model.dto.Bps;
import com.dsms.modules.monitor.model.dto.ClusterStatusDTO;
import com.dsms.modules.monitor.model.dto.Ops;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "集群概览信息", description = "集群概览信息")
public class OverviewDTO {

    @ApiModelProperty("集群健康状态")
    private String clusterStatus;
    @ApiModelProperty("集群健康状态详情")
    private String clusterStatusDetail;
    @ApiModelProperty("osd信息")
    private ClusterStatusDTO.OsdDTO osd;
    @ApiModelProperty("节点信息")
    private List<NodeDTO> node;
    @ApiModelProperty("ops")
    private Ops ops;
    @ApiModelProperty("bps")
    private Bps bps;
    @ApiModelProperty("集群已使用存储容量占比")
    private Double capacityUsage;


    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class NodeDTO {
        @ApiModelProperty("节点名称")
        private String nodeName;
        @ApiModelProperty("节点ip地址")
        private String ipAddress;
        @ApiModelProperty("节点状态")
        private Integer status;
        @ApiModelProperty("节点cpu使用占比")
        private Double cpuUsage;
        @ApiModelProperty("节点内存使用占比")
        private Double memoryUsage;
    }
}
