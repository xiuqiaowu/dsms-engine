/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.rbd.model.vo;

import com.dsms.common.constant.NameSchemeEnum;
import com.dsms.common.validator.GeneralParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ApiModel(value = "创建存储卷")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RbdCreateVO {
    @ApiModelProperty(value = "存储池名称", required = true, example = "pool1")
    @NotBlank(message = "存储池名称不能为空")
    private String poolName;
    @ApiModelProperty(value = "数据池名称", required = true, example = "eraPool1")
    private String dataPoolName;
    @ApiModelProperty(value = "存储卷名称", required = true, example = "rbd1")
    @NotBlank(message = "存储卷名称不能为空")
    @GeneralParam(nameSchemeEnum = NameSchemeEnum.STORAGE_VOLUME)
    private String rbdName;
    @ApiModelProperty(value = "存储卷容量", required = true, example = "5.0")
    @NotNull(message = "存储卷容量不能为空")
    @Min(value = 0, message = "存储卷容量不可小于0")
    private Double rbdSize;
    @ApiModelProperty(value = "是否基于纠删码池创建", required = true, example = "false")
    private Boolean basedOnEra;
}
