/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.node.model.vo;

import com.dsms.common.model.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

@ApiModel(value = "节点分页VO")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NodePageVO extends PageParam {

    @ApiModelProperty(value = "节点名称", example = "node1")
    private String nodeName;

    @ApiModelProperty(value = "节点状态(0-离线，1-在线)", example = "0")
    @Range(min = 0, max = 1, message = "节点状态不存在")
    private Integer nodeStatus;

}
