/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.node.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class NodeDeviceManageDto {
    public static final String DEVICE_PATH_PREFIX = "/dev/";

    private Integer osdId;
    //the host name of osd
    private String host;
    //the device path of osd in system
    private String path;

    public NodeDeviceManageDto(int osdId, String host, String path) {
        this.osdId = osdId;
        this.host = host;
        this.path = DEVICE_PATH_PREFIX + path;
    }

    public NodeDeviceManageDto(String host, String path) {
        this.host = host;
        this.path = path;
    }
}
