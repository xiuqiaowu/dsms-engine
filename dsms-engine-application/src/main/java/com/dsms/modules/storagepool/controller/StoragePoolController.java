/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.storagepool.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dsms.common.constant.StoragePoolTypeEnum;
import com.dsms.common.model.Result;
import com.dsms.dfsbroker.node.model.dto.NodeDto;
import com.dsms.dfsbroker.osd.ecprofile.model.dto.EcProfileDto;
import com.dsms.dfsbroker.storagepool.model.StoragePool;
import com.dsms.dfsbroker.storagepool.model.dto.*;
import com.dsms.dfsbroker.storagepool.service.IStoragePoolService;
import com.dsms.modules.storagepool.model.vo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/pool")
@Api(tags = "存储池模块")
public class StoragePoolController {
    @Autowired
    IStoragePoolService storagePoolService;

    @ApiOperation("存储池模块-指定存储池名称获取存储池信息")
    @PostMapping("/get")
    public Result<StoragePool> get(@Validated @RequestBody StoragePoolGetVO storagePoolGetVO) {
        return Result.OK(storagePoolService.get(storagePoolGetVO.getPoolName()));
    }

    @ApiOperation("存储池模块-获取存储池列表信息")
    @PostMapping("/list")
    public Result<Page<StoragePool>> list(@RequestBody StoragePoolListVO storagePoolListVO) {
        Page<StoragePool> storagePoolList = storagePoolService.list(storagePoolListVO);
        return Result.OK(storagePoolList);
    }

    @ApiOperation("存储池模块-创建存储池")
    @PostMapping("/create_pool")
    public Result<Boolean> create(@Validated @RequestBody StoragePoolCreateVO storagePoolCreateVO) {
        StoragePoolCreateDTO storagePoolCreateDTO = null;
        if (storagePoolCreateVO.getPoolType() == StoragePoolTypeEnum.REPLICATED.getCode()) {
            storagePoolCreateDTO = new ReplicatedCreateDTO(
                    storagePoolCreateVO.getPoolName(),
                    storagePoolCreateVO.getPoolType(),
                    storagePoolCreateVO.getPgNum(),
                    storagePoolCreateVO.getPgpNum(),
                    storagePoolCreateVO.getReplicaNum(),
                    storagePoolCreateVO.getPoolName()
            );

        } else if (storagePoolCreateVO.getPoolType() == StoragePoolTypeEnum.ERASURE.getCode()) {
            storagePoolCreateDTO = new ErasureCreateDTO(
                    storagePoolCreateVO.getPoolName(),
                    storagePoolCreateVO.getPoolType(),
                    storagePoolCreateVO.getPgNum(),
                    storagePoolCreateVO.getPgpNum(),
                    new EcProfileDto(
                            storagePoolCreateVO.getPoolName(),
                            storagePoolCreateVO.getEcProfile().getDataChunks(),
                            storagePoolCreateVO.getEcProfile().getCodeChunks(),
                            storagePoolCreateVO.getEcProfile().getPlugin()
                    )
            );
        }
        return Result.OK(storagePoolService.create(storagePoolCreateDTO));
    }

    @ApiOperation("存储池模块-获取存储池所有已使用的节点、磁盘")
    @PostMapping("/list_used_disk")
    public Result<List<NodeDto>> listUsedDisk(@RequestBody StoragePoolNodeManageVO storagePoolNodeManageVO) {
        List<NodeDto> storagePoolList = storagePoolService.listUsedDisk(storagePoolNodeManageVO.getPoolName());
        return Result.OK(storagePoolList);
    }

    @ApiOperation("存储池模块-获取存储池在某节点未使用的磁盘")
    @PostMapping("/list_unused_disk")
    public Result<NodeDto> listUnusedDisk(@RequestBody StoragePoolNodeManageVO storagePoolNodeManageVO) {
        NodeDto storagePoolList = storagePoolService.listUnusedDisk(storagePoolNodeManageVO.getPoolName(), storagePoolNodeManageVO.getNodeName());
        return Result.OK(storagePoolList);
    }

    @ApiOperation("存储池模块-存储池添加节点")
    @PostMapping("/add_node")
    public Result<Boolean> addNode(@Validated @RequestBody StoragePoolNodeManageVO storagePoolNodeManageVO) {
        StoragePoolNodeManageDTO storagePoolNodeManageDTO = new StoragePoolNodeManageDTO(
                storagePoolNodeManageVO.getPoolName(),
                storagePoolNodeManageVO.getNodeNames()
        );
        return Result.OK(storagePoolService.addNode(storagePoolNodeManageDTO));
    }

    @ApiOperation("存储池模块-存储池移除节点")
    @PostMapping("/remove_node")
    public Result<Boolean> removeNode(@Validated @RequestBody StoragePoolNodeManageVO storagePoolNodeManageVO) {
        StoragePoolNodeManageDTO storagePoolNodeManageDTO = new StoragePoolNodeManageDTO(
                storagePoolNodeManageVO.getPoolName(),
                storagePoolNodeManageVO.getNodeName()
        );
        return Result.OK(storagePoolService.removeNode(storagePoolNodeManageDTO));
    }

    @ApiOperation("存储池模块-存储池添加磁盘")
    @PostMapping("/add_disk")
    public Result<Boolean> addDisk(@Validated @RequestBody StoragePoolDiskManageVO storagePoolDiskManageVO) {
        StoragePoolDiskManageDTO storagePoolDiskManageDTO = new StoragePoolDiskManageDTO(
                storagePoolDiskManageVO.getPoolName(),
                storagePoolDiskManageVO.getOsd()
        );
        return Result.OK(storagePoolService.addDisk(storagePoolDiskManageDTO));
    }

    @ApiOperation("存储池模块-存储池移除磁盘")
    @PostMapping("/remove_disk")
    public Result<Boolean> removeDisk(@Validated @RequestBody StoragePoolDiskManageVO storagePoolDiskManageVO) {
        StoragePoolDiskManageDTO storagePoolDiskManageDTO = new StoragePoolDiskManageDTO(
                storagePoolDiskManageVO.getPoolName(),
                storagePoolDiskManageVO.getOsd()
        );
        return Result.OK(storagePoolService.removeDisk(storagePoolDiskManageDTO));
    }

    @ApiOperation("存储池模块-删除存储池")
    @PostMapping("/delete_pool")
    public Result<Boolean> deletePool(@Validated @RequestBody StoragePoolDeleteVO storagePoolDeleteVO) {
        StoragePoolDeleteDTO storagePoolDeleteDTO = new StoragePoolDeleteDTO().setPoolName(storagePoolDeleteVO.getPoolName());
        return Result.OK(storagePoolService.deletePool(storagePoolDeleteDTO));
    }
}
