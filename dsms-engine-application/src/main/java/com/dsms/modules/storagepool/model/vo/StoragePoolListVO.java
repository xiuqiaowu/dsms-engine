/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.storagepool.model.vo;

import com.dsms.common.model.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@ApiModel(value = "获取存储池列表VO")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StoragePoolListVO extends PageParam {
    @ApiModelProperty(value = "存储池类型", example = "1(replicated) 3(erasure)")
    private Integer poolType;

    @ApiModelProperty(value = "存储池文件系统", example = "cephfs")
    private String fileSystem;

    @ApiModelProperty(value = "存储卷列表", example = "['rbd1','rbd2']")
    private List<String> rbd;

    @ApiModelProperty(value = "存储池名称", example = "pool1")
    private String poolName;

    @ApiModelProperty(value = "存储池是否可用", example = "true")
    private Boolean isAvailable;
}
