/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.mail.service;

import com.dsms.modules.alert.entity.AlertApprise;
import com.dsms.modules.mail.model.MailMessage;

/**
 * Email operation service
 */
public interface IMailSendService {

    /**
     * Initializes the mail account
     */
    void initMailAccount();

    /**
     * Update the mail account
     */
    void updateMailAccount(AlertApprise alertApprise);


    /**
     * Send mail to multiple target
     * @param mailMessage mail config
     */
    void sendMail(MailMessage mailMessage);

}
