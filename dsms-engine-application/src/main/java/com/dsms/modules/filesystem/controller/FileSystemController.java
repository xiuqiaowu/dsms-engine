/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.filesystem.controller;

import com.dsms.common.model.Result;
import com.dsms.dfsbroker.filesystem.FileSystem;
import com.dsms.dfsbroker.filesystem.model.dto.FileSystemDTO;
import com.dsms.dfsbroker.filesystem.service.IFileSystemService;
import com.dsms.modules.filesystem.model.FileSystemCreateVO;
import com.dsms.modules.filesystem.model.FileSystemRemoveVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/cephfs")
@Api(tags = "文件系统模块")
public class FileSystemController {

    @Autowired
    private IFileSystemService fileSystemService;

    @ApiOperation("文件系统模块-获取文件系统信息")
    @PostMapping("/list")
    public Result<List<FileSystem>> list() {
        List<FileSystem> fileSystems = fileSystemService.list();

        return Result.OK(fileSystems);
    }

    @ApiOperation("文件系统模块-新增文件系统")
    @PostMapping("/create_fs")
    public Result<Boolean> createFs(@Validated @RequestBody FileSystemCreateVO fileSystemCreateVO) {
        FileSystemDTO fileSystemDTO = new FileSystemDTO(fileSystemCreateVO.getFsName(), fileSystemCreateVO.getMetadataPool(), fileSystemCreateVO.getDataPool(), null);
        return Result.OK(fileSystemService.createFs(fileSystemDTO));
    }

    @ApiOperation("文件系统模块-删除文件系统")
    @PostMapping("/delete_fs")
    public Result<Boolean> deleteFs(@Validated @RequestBody FileSystemRemoveVO fileSystemRemoveVO) {
        FileSystemDTO fileSystemDTO = new FileSystemDTO(fileSystemRemoveVO.getFsName());
        return Result.OK(fileSystemService.deleteFs(fileSystemDTO));
    }


}
