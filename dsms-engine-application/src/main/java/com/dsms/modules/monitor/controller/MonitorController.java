/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.monitor.controller;

import com.dsms.common.model.Result;
import com.dsms.modules.monitor.model.dto.*;
import com.dsms.modules.monitor.model.vo.*;
import com.dsms.modules.monitor.service.IMonitorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/monitor")
@Api(tags = "监控中心模块")
public class MonitorController {
    @Autowired
    IMonitorService monitorService;

    @ApiOperation("监控中心模块-获取集群状态")
    @PostMapping("/list_mon_status")
    public Result<ClusterStatusDTO> listClusterStatusMon() {
        return monitorService.listClusterStatusMon();
    }

    @ApiOperation("监控中心模块-获取集群性能")
    @PostMapping("/get_cluster_mon")
    public Result<ClusterMonitorDTO> getClusterPerformanceMon(@Validated @RequestBody DurationVO durationVO) {
        return monitorService.getClusterPerformanceMon(durationVO);
    }

    @ApiOperation("监控中心模块-获取节点性能")
    @PostMapping("/get_node_mon")
    public Result<NodeMonitorDTO> getNodePerformanceMon(@Validated @RequestBody NodePerformanceVO nodePerformanceVO) {
        return monitorService.getNodePerformanceMon(nodePerformanceVO);
    }

    @ApiOperation("监控中心模块-获取存储池性能")
    @PostMapping("/get_pool_mon")
    public Result<StoragePoolMonitorDTO> getStoragePoolPerformanceMon(@Validated @RequestBody StoragePoolPerformanceVO storagePoolPerformanceVO) {
        return monitorService.getStoragePoolPerformanceMon(storagePoolPerformanceVO);
    }

    @ApiOperation("监控中心模块-开启对某存储池下所有存储卷性能监控的支持")
    @PostMapping("/enable_rbd_mon")
    public Result<Boolean> enableRbdMon(@Validated @RequestBody EnableRbdMonitorVO enableRbdMonitorVO) {
        return monitorService.enableRbdMon(enableRbdMonitorVO);
    }

    @ApiOperation("监控中心模块-获取存储卷性能")
    @PostMapping("/get_rbd_mon")
    public Result<RbdMonitorDTO> getRbdMon(@Validated @RequestBody RbdMonitorVO rbdMonitorVO) {
        return monitorService.getRbdMon(rbdMonitorVO);
    }

    @ApiOperation("监控中心模块-获取文件系统性能")
    @PostMapping("/get_cephfs_mon")
    public Result<CephFSMonitorDTO> getCephFSMon(@Validated @RequestBody DurationVO durationVO) {
        return monitorService.getCephFSMon(durationVO);
    }

    @ApiOperation("监控中心模块-获取osd性能")
    @PostMapping("/get_osd_mon")
    public Result<OsdMonitorDTO> getOsdMon(@Validated @RequestBody OsdMonitorVO osdMonitorVO) {
        return monitorService.getOsdMon(osdMonitorVO);
    }
}
