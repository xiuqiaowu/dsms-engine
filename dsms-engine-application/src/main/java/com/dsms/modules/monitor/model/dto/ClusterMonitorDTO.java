/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.monitor.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class ClusterMonitorDTO {
    //cluster's read and write ops
    private Ops ops;
    //The change curve of pg quantity in various states
    private PG pg;
    //pg recovery rate
    private double[][] recoveryOps;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PG {
        //total pg in the cluster
        private double[][] totalPg;
        //pg in the cluster with status active
        private double[][] activePg;
        //pg in the cluster with status inactive
        private double[][] inactivePg;
        //pg in the cluster with status undersized
        private double[][] undersizedPg;
        //pg in the cluster with status degraded
        private double[][] degradedPg;
        //pg in the cluster with status inconsistent
        private double[][] inconsistentPg;
        //pg in the cluster with status down
        private double[][] downPg;
        //pg in the cluster with status unknown
        private double[][] unknownPg;
    }
}
