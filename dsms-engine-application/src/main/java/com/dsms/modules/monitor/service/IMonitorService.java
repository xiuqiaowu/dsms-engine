/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.monitor.service;

import com.dsms.common.model.Result;
import com.dsms.modules.monitor.model.dto.*;
import com.dsms.modules.monitor.model.vo.*;

public interface IMonitorService {
    /**
     * get cluster's status monitor data
     *
     * @return Result<ClusterStatusDTO>
     */
    Result<ClusterStatusDTO> listClusterStatusMon();

    /**
     * get cluster's performance monitor data
     *
     * @return Result<ClusterMonitorDTO>
     */
    Result<ClusterMonitorDTO> getClusterPerformanceMon(DurationVO durationVO);

    /**
     * get node's performance monitor data
     *
     * @return Result<NodeMonitorDTO>
     */
    Result<NodeMonitorDTO> getNodePerformanceMon(NodePerformanceVO nodePerformanceVO);

    /**
     * get storage pool's performance monitor data
     *
     * @return Result<StoragePoolMonitorDTO>
     */
    Result<StoragePoolMonitorDTO> getStoragePoolPerformanceMon(StoragePoolPerformanceVO storagePoolPerformanceVO);

    /**
     * enable performance monitoring for all volumes in a storage pool
     *
     * @return Result<Boolean>
     */
    Result<Boolean> enableRbdMon(EnableRbdMonitorVO enableRbdMonitorVO);

    /**
     * get storage pool's performance monitor data
     *
     * @return Result<RbdMonitorDTO>
     */
    Result<RbdMonitorDTO> getRbdMon(RbdMonitorVO rbdMonitorVO);

    /**
     * get ceph's fileSystem(only one in cluster) performance monitor data
     *
     * @return Result<CephFSMonitorDTO>
     */
    Result<CephFSMonitorDTO> getCephFSMon(DurationVO durationVO);

    /**
     * get osd's performance monitor data
     *
     * @return Result<OsdMonitorDTO>
     */
    Result<OsdMonitorDTO> getOsdMon(OsdMonitorVO osdMonitorVO);
}
