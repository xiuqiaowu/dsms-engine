/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.storagepool.request;

import com.alibaba.fastjson2.JSON;
import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.dfsbroker.common.RequestUrlEnum;
import org.springframework.http.HttpMethod;
import org.springframework.util.ObjectUtils;

import java.util.HashMap;
import java.util.Map;

public class DeleteStoragePoolRequest extends RemoteRequest {

    public static final String DELETE_POOL_SUCCESS = "pool '%s' removed";

    public DeleteStoragePoolRequest(String poolName) {
        if (ObjectUtils.isEmpty(poolName)) {
            throw new IllegalArgumentException("pool name must not be blank");
        }

        Map<String, Object> requestParam = new HashMap<>(8);
        requestParam.put("prefix", "osd pool delete");
        requestParam.put("pool", poolName);
        requestParam.put("pool2", poolName);
        requestParam.put("yes_i_really_really_mean_it", true);


        this.setUrlPrefix(RequestUrlEnum.REQUEST_URL.getUrlPrefix());
        this.setHttpMethod(HttpMethod.POST);
        this.setRequestBody(JSON.toJSONString(requestParam));
    }
}
