/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.storagepool.request;

import com.alibaba.fastjson2.JSON;
import com.dsms.common.constant.StoragePoolTypeEnum;
import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.common.util.ObjectUtil;
import com.dsms.dfsbroker.common.RequestUrlEnum;
import com.dsms.dfsbroker.storagepool.model.dto.ErasureCreateDTO;
import com.dsms.dfsbroker.storagepool.model.dto.ReplicatedCreateDTO;
import com.dsms.dfsbroker.storagepool.model.dto.StoragePoolCreateDTO;
import org.springframework.http.HttpMethod;

import java.util.HashMap;
import java.util.Map;

public class CreateStoragePoolRequest extends RemoteRequest {

    public static final String Create_POOL_SUCCESS = "pool '%s' created";

    public CreateStoragePoolRequest(StoragePoolCreateDTO pool) {
        if (ObjectUtil.checkObjFieldContainNull(pool)) {
            throw new IllegalArgumentException("pool's parameters should not be blank,please check and try again");
        }

        Map<String, Object> requestParam = new HashMap<>(8);
        requestParam.put("prefix", "osd pool create");
        requestParam.put("pool", pool.getPoolName());
        requestParam.put("pg_num", pool.getPgNum());
        requestParam.put("pgs_num", pool.getPgpNum());

        if (pool instanceof ReplicatedCreateDTO) {
            requestParam.put("size", ((ReplicatedCreateDTO) pool).getSize());
            requestParam.put("rule", ((ReplicatedCreateDTO) pool).getRuleName());
        } else if (pool instanceof ErasureCreateDTO) {
            requestParam.put("pool_type", StoragePoolTypeEnum.ERASURE.getType());
            requestParam.put("erasure_code_profile", pool.getPoolName());
        } else {
            throw new IllegalArgumentException("unknown pool type");
        }

        this.setUrlPrefix(RequestUrlEnum.REQUEST_URL.getUrlPrefix());
        this.setHttpMethod(HttpMethod.POST);
        this.setRequestBody(JSON.toJSONString(requestParam));
    }
}
