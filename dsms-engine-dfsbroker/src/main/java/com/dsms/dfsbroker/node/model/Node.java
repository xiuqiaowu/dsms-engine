/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.node.model;


import com.dsms.dfsbroker.node.model.remote.ResponseMon;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@ApiModel(value = "Node对象", description = "集群节点")
public class Node implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("节点version")
    private String version;

    @ApiModelProperty("节点id")
    private String nodeId;

    @ApiModelProperty("节点ip")
    private String nodeIp;

    @ApiModelProperty("节点名称")
    private String nodeName;

    @ApiModelProperty("节点状态(0-离线，1-在线)")
    private Integer nodeStatus;

    @ApiModelProperty("节点磁盘")
    private List<Device> devices;

    @ApiModelProperty("节点已用磁盘数")
    private Integer nodeUsedDevice;

    @ApiModelProperty("节点总磁盘数")
    private Integer nodeAllDevice;

    @ApiModelProperty("节点已被存储池使用磁盘数")
    private Integer poolUsedDevice;

    public static List<Node> monParseNode(List<ResponseMon> responseMons) {
        List<Node> nodes = new ArrayList<>();

        for (ResponseMon responseMon : responseMons) {
            Node nodeTmp = new Node();
            nodeTmp.setNodeName(responseMon.getName());
            nodeTmp.setNodeIp(responseMon.getAddr());
            nodes.add(nodeTmp);
        }

        return nodes;
    }
}
