/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.node.model.remote;

import com.alibaba.fastjson2.annotation.JSONField;
import lombok.Data;

import java.time.LocalDateTime;


/**
 * dsms-storage (orch osd rm status) execute result
 */
@Data
public class OsdRmStatusResult {

    @JSONField(name = "drain_done_at")
    private String drainDoneAt;
    @JSONField(name = "drain_started_at")
    private String drainStartedAt;
    @JSONField(name = "drain_stopped_at")
    private String drainStoppedAt;
    private boolean draining;
    private boolean force;
    private String hostname;
    @JSONField(name = "osd_id")
    private Integer osdId;
    @JSONField(name = "process_started_at")
    private LocalDateTime processStartedAt;
    private boolean replace;
    private boolean started;
    private boolean stopped;
}
