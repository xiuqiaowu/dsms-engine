/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.node.model;

import com.dsms.common.constant.DeviceStatusEnum;
import com.dsms.common.constant.RbdDeviceTypeEnum;
import com.dsms.dfsbroker.node.model.remote.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@ApiModel(value = "磁盘对象", description = "节点上磁盘")
public class Device implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("磁盘id")
    private String deviceId;

    @ApiModelProperty("磁盘路径")
    private String devicePath;

    @ApiModelProperty("磁盘pg数量")
    private Integer devicePgs;

    @ApiModelProperty("磁盘类型")
    private String deviceType;

    @ApiModelProperty("磁盘用量")
    private Long deviceUsedSize;

    @ApiModelProperty("磁盘容量")
    private String deviceTotalSize;

    @ApiModelProperty("磁盘状态(0-不可用，1-可用)")
    private Integer deviceStatus;

    @ApiModelProperty("osdId")
    private Integer osdId;

    @ApiModelProperty("osdStatus")
    private String osdStatus;

    @ApiModelProperty("占用此磁盘的存储池名称")
    private String usedPool;

    @ApiModelProperty("此磁盘所属集群的fsid")
    private String clusterFsid;

    public static List<Device> osdDfResultParseDevice(OsdDfResult osdDfResult) {
        List<Device> devices = new ArrayList<>();

        List<ResponseNode> responseNodes = osdDfResult.getNodes();
        for (ResponseNode responseNode : responseNodes) {
            Device deviceTmp = new Device();
            deviceTmp.setOsdId(responseNode.getId());
            deviceTmp.setDeviceUsedSize(responseNode.getKbUsed() * 1024);
            deviceTmp.setDevicePgs(responseNode.getPgs());
            deviceTmp.setOsdStatus(responseNode.getStatus());
            devices.add(deviceTmp);
        }

        return devices;
    }

    public static List<Device> orchDeviceLsResultParseDevice(OrchDeviceLsResult orchDeviceLsResult) {
        List<Device> devices = new ArrayList<>();

        List<ResponseDevice> responseDevices = orchDeviceLsResult.getDevices();
        for (ResponseDevice responseDevice : responseDevices) {
            //filter devices mapped by rbd
            if (RbdDeviceTypeEnum.isRbdDeviceType(responseDevice.getPath())) {
                continue;
            }
            Device deviceTmp = new Device();
            deviceTmp.setDeviceId(responseDevice.getDeviceId());
            deviceTmp.setDevicePath(responseDevice.getPath());
            deviceTmp.setDeviceType(responseDevice.getHumanReadableType());
            deviceTmp.setDeviceTotalSize(responseDevice.getSysApi().getHumanReadableSize());
            deviceTmp.setDeviceStatus(responseDevice.isAvailable() ? DeviceStatusEnum.AVAILABLE.getStatus() : DeviceStatusEnum.NOT_AVAILABLE.getStatus());
            List<Lvs> lvs = responseDevice.getLvs();
            for (Lvs lv : lvs) {
                deviceTmp.setOsdId(Integer.valueOf(lv.getOsdId()));
                deviceTmp.setClusterFsid(lv.getClusterFsid());
            }
            devices.add(deviceTmp);
        }

        return devices;
    }

}
