/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.rbd.request;

import com.alibaba.fastjson2.JSON;
import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.dfsbroker.common.RequestUrlEnum;
import org.springframework.http.HttpMethod;
import org.springframework.util.ObjectUtils;

import java.util.HashMap;
import java.util.Map;

public class EnableRbdMonRequest extends RemoteRequest {

    public EnableRbdMonRequest(String poolName) {
        if (ObjectUtils.isEmpty(poolName)) {
            throw new IllegalArgumentException("poolName can not be empty");
        }
        Map<String, Object> requestParam = new HashMap<>(4);
        requestParam.put("prefix", "config set");
        requestParam.put("who", "mgr");
        requestParam.put("name", "mgr/prometheus/rbd_stats_pools");
        requestParam.put("value", poolName);

        this.setUrlPrefix(RequestUrlEnum.REQUEST_URL.getUrlPrefix());
        this.setHttpMethod(HttpMethod.POST);
        this.setRequestBody(JSON.toJSONString(requestParam));
    }
}
