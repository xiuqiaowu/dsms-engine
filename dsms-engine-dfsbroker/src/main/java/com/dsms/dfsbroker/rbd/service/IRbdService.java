/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.rbd.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dsms.common.model.PageParam;
import com.dsms.common.model.Result;
import com.dsms.dfsbroker.rbd.model.Rbd;
import com.dsms.dfsbroker.rbd.model.dto.RbdCreateDTO;
import com.dsms.dfsbroker.rbd.model.dto.RbdDeleteDTO;
import com.dsms.dfsbroker.rbd.model.dto.RbdGetDTO;
import com.dsms.dfsbroker.rbd.model.dto.RbdUpdateDTO;

public interface IRbdService {
    /**
     * get a dsms-storage single rbd info
     *
     * @return rbd info
     */
    Rbd get(RbdGetDTO rbdGetDTO);

    /**
     * get dsms-storage all rbd info
     *
     * @return rbd info list
     */
    Page<Rbd> list(PageParam pageParam);

    /**
     * create dsms-storage rbd
     *
     * @return boolean
     */
    boolean create(RbdCreateDTO rbdCreateDTO);

    /**
     * delete dsms-storage rbd
     *
     * @return boolean
     */
    boolean delete(RbdDeleteDTO rbdDeleteDTO);

    /**
     * update the capacity of dsms-storage rbd
     *
     * @return boolean
     */
    boolean update(RbdUpdateDTO rbdUpdateDTO);

    /**
     * download dsms-storage's ceph.client.admin.keyring
     *
     * @return return a map
     * the value of key named "md5" is the md5 value of the certificate
     * The value of key named "ceph.client.admin.keyring" is the byte array of the certificate
     */
    Result<Object> downloadKey();
}
