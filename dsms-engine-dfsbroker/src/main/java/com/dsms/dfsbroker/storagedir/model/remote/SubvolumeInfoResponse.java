/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.storagedir.model.remote;


import com.alibaba.fastjson2.annotation.JSONField;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * dsms-storage (subvolume info -f json) execute result
 */
@NoArgsConstructor
@Data
public class SubvolumeInfoResponse {

    /**
     * access time of subvolume group path in the format “YYYY-MM-DD HH:MM:SS”
     */
    @JSONField(name = "atime")
    private String atime;
    /**
     * quota used in percentage if quota is set, else displays “undefined”
     */
    @JSONField(name = "bytes_pcent")
    private String bytesPcent;
    /**
     * quota size in bytes if quota is set, else displays “infinite”
     */
    @JSONField(name = "bytes_quota")
    private String bytesQuota;
    /**
     * current used size of the subvolume group in bytes
     */
    @JSONField(name = "bytes_used")
    private Long bytesUsed;
    /**
     * time of creation of subvolume group in the format “YYYY-MM-DD HH:MM:SS”
     */
    @JSONField(name = "created_at")
    private String createdAt;
    /**
     * change time of subvolume group path in the format “YYYY-MM-DD HH:MM:SS”
     */
    @JSONField(name = "ctime")
    private String ctime;
    /**
     * data pool the subvolume group belongs to
     */
    @JSONField(name = "data_pool")
    private String dataPool;
    /**
     * subvolume features (snapshot-clone,snapshot-autoprotect,snapshot-retention)
     */
    @JSONField(name = "features")
    private List<String> features;
    /**
     * gid of subvolume group path
     */
    @JSONField(name = "gid")
    private Integer gid;
    /**
     * mode of subvolume group path
     */
    @JSONField(name = "mode")
    private Integer mode;
    /**
     * list of monitor addresses
     */
    @JSONField(name = "mon_addrs")
    private List<String> monAddrs;
    /**
     * modification time of subvolume group path in the format “YYYY-MM-DD HH:MM:SS”
     */
    @JSONField(name = "mtime")
    private String mtime;
    /**
     * subvolume path
     */
    @JSONField(name = "path")
    private String path;
    /**
     * subvolume pool namespace
     */
    @JSONField(name = "pool_namespace")
    private String poolNamespace;
    /**
     * subvolume state(complete)
     */
    @JSONField(name = "state")
    private String state;
    /**
     * subvolume type(subvolume)
     */
    @JSONField(name = "type")
    private String type;
    /**
     * uid of subvolume group path
     */
    @JSONField(name = "uid")
    private Integer uid;
}
