/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.storagedir.model.dto;


import com.dsms.common.constant.NameSchemeEnum;
import com.dsms.common.validator.DsmsValidateGroup;
import com.dsms.common.validator.GeneralParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StorageDirDTO {

    @NotBlank(message = "文件系统名称不能为空", groups = {DsmsValidateGroup.Create.class, DsmsValidateGroup.Remove.class,})
    private String volName;

    @NotBlank(message = "存储目录名称不能为空", groups = {DsmsValidateGroup.Create.class, DsmsValidateGroup.Remove.class})
    @GeneralParam(nameSchemeEnum = NameSchemeEnum.STORAGE_DIR, groups = {DsmsValidateGroup.Create.class})
    private String subName;

    private String authId;

    public StorageDirDTO(String volName) {
        this.volName = volName;
    }

    public StorageDirDTO(String volName, String subName) {
        this.volName = volName;
        this.subName = subName;
    }

}
