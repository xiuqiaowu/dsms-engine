/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.storagedir.model.remote;


import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * dsms-storage (subvolume authorized_list -f json) execute result
 */
@NoArgsConstructor
@Data
public class SubvolumeAuthListResponse {

    private String authId;
    private String accessLevel;

    public static List<SubvolumeAuthListResponse> parseResponseToObject(JSONArray subvolumeLsResponses) {
        List<SubvolumeAuthListResponse> subvolumeAuthListResponses = new ArrayList<>();

        List<JSONObject> subvolumeAuthList = subvolumeLsResponses.toList(JSONObject.class);
        for (JSONObject subvolumeAuth : subvolumeAuthList) {
            SubvolumeAuthListResponse sub = new SubvolumeAuthListResponse();
            Set<Map.Entry<String, Object>> entries = subvolumeAuth.entrySet();
            if (!entries.isEmpty()) {
                entries.forEach(tmp -> {
                    sub.setAuthId(tmp.getKey());
                    sub.setAccessLevel((String) tmp.getValue());
                });
                subvolumeAuthListResponses.add(sub);
            }

        }
        return subvolumeAuthListResponses;
    }
}
