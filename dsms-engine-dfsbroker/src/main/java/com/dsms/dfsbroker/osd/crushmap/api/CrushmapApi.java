/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.osd.crushmap.api;

import com.dsms.common.constant.RemoteResponseStatusEnum;
import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.common.remotecall.model.RemoteResponse;
import com.dsms.dfsbroker.common.CommandDirector;
import com.dsms.dfsbroker.common.api.CommonApi;
import com.dsms.dfsbroker.osd.crushmap.model.remote.CrushmapBucket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Component
public class CrushmapApi {
    public static final int MAX_RETRIES = CommonApi.DEFAULT_RETRY_TIMES;

    @Autowired
    private CommonApi commonApi;

    /**
     * create bucket in crushmap
     *
     * @return RemoteResponse
     */
    public RemoteResponse createBucket(RemoteRequest remoteRequest, String bucketName, int type, String root) throws Throwable {
        CommandDirector.constructCreateBucketRequest(remoteRequest, bucketName, type, root);
        return commonApi.remoteCall(remoteRequest, MAX_RETRIES, RemoteResponse.class);
    }

    /**
     * create rule in crushmap
     *
     * @return RemoteResponse
     */
    public RemoteResponse createRule(RemoteRequest remoteRequest, String ruleName) throws Throwable {
        CommandDirector.constructCreateRuleRequest(remoteRequest, ruleName);
        return commonApi.remoteCall(remoteRequest, MAX_RETRIES, RemoteResponse.class);
    }

    /**
     * get the result of update crushmap
     *
     * @return RemoteResponse
     */
    public RemoteResponse getUpdateCrushmapResult(RemoteRequest remoteRequest, String requestId) throws Throwable {
        CommandDirector.constructGetRequest(remoteRequest, requestId);

        boolean flag = true;
        int pendingRetrys = 0;
        RemoteResponse response = null;
        while (flag && pendingRetrys < MAX_RETRIES) {
            response = commonApi.remoteCall(remoteRequest, MAX_RETRIES, RemoteResponse.class);
            if (!Objects.equals(response.getState(), RemoteResponseStatusEnum.PENDING.getMessage())) {
                flag = false;
            }
            pendingRetrys++;
            TimeUnit.SECONDS.sleep(1);
        }
        return response;
    }

    /**
     * get buckets in crushmap
     *
     * @return RemoteResponse
     */
    public CrushmapBucket getCrushmapBuckets(RemoteRequest remoteRequest) throws Throwable {
        CommandDirector.constructGetCrushmapBucketsRequest(remoteRequest);
        return commonApi.remoteCallRequest(remoteRequest, MAX_RETRIES, CrushmapBucket.class);
    }

    /**
     * delete bucket in crushmap
     *
     * @return RemoteResponse
     */
    public RemoteResponse delBucket(RemoteRequest remoteRequest, String bucketName) throws Throwable {
        CommandDirector.constructDelBucketRequest(remoteRequest, bucketName);
        RemoteResponse remoteResponse = commonApi.remoteCall(remoteRequest, MAX_RETRIES, RemoteResponse.class);
        CommandDirector.constructGetRequest(remoteRequest, remoteResponse.getId());
        return commonApi.remoteCall(remoteRequest, MAX_RETRIES, RemoteResponse.class);
    }

    /**
     * delete rule in crushmap
     *
     * @return RemoteResponse
     */
    public RemoteResponse delCrushRule(RemoteRequest remoteRequest, String ruleName) throws Throwable {
        CommandDirector.constructDelRuleRequest(remoteRequest, ruleName);
        RemoteResponse remoteResponse = commonApi.remoteCall(remoteRequest, MAX_RETRIES, RemoteResponse.class);
        CommandDirector.constructGetRequest(remoteRequest, remoteResponse.getId());
        return commonApi.remoteCall(remoteRequest, MAX_RETRIES, RemoteResponse.class);
    }

}
