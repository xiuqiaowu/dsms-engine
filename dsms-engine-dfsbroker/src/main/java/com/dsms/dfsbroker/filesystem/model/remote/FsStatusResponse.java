/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.filesystem.model.remote;

import com.alibaba.fastjson2.annotation.JSONField;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * dsms-storage (fs status -f json) execute result
 */
@NoArgsConstructor
@Data
public class FsStatusResponse {
    @JSONField(name = "clients")
    private List<ClientsDTO> clients;
    @JSONField(name = "mds_version")
    private String mdsVersion;
    @JSONField(name = "mdsmap")
    private List<MdsmapDTO> mdsmap;
    @JSONField(name = "pools")
    private List<PoolsDTO> pools;

    @NoArgsConstructor
    @Data
    public static class ClientsDTO {
        @JSONField(name = "clients")
        private Integer clients;
        @JSONField(name = "fs")
        private String fs;
    }

    @NoArgsConstructor
    @Data
    public static class MdsmapDTO {
        @JSONField(name = "dns")
        private Integer dns;
        @JSONField(name = "inos")
        private Integer inos;
        @JSONField(name = "name")
        private String name;
        @JSONField(name = "rank")
        private Integer rank;
        @JSONField(name = "rate")
        private Integer rate;
        @JSONField(name = "state")
        private String state;
    }

    @NoArgsConstructor
    @Data
    public static class PoolsDTO {
        @JSONField(name = "avail")
        private Long avail;
        @JSONField(name = "id")
        private Integer id;
        @JSONField(name = "name")
        private String name;
        @JSONField(name = "type")
        private String type;
        @JSONField(name = "used")
        private Long used;
    }
}
