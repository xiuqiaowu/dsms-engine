/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.filesystem;


import com.dsms.common.constant.FileSystemPoolTypeEnum;
import com.dsms.common.constant.FileSystemStatusEnum;
import com.dsms.dfsbroker.filesystem.model.remote.FsStatusResponse;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.ObjectUtils;

@Data
@ApiModel(value = "FileSystem对象", description = "文件系统")
@AllArgsConstructor
@NoArgsConstructor
public class FileSystem {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("文件系统名称")
    private String fsName;

    @ApiModelProperty("元数据存储池")
    private String metadataPoolName;

    @ApiModelProperty("数据存储池id")
    private Integer dataPoolId;

    @ApiModelProperty("数据存储池")
    private String dataPoolName;

    @ApiModelProperty("文件系统用量")
    private Long dataPoolUsedSize;

    @ApiModelProperty("文件系统容量")
    private Long dataPoolTotalSize;

    @ApiModelProperty("文件系统状态(0-停用,1-运行)")
    private Integer status;

    public static FileSystem fsStatusResponseParseFileSystem(FsStatusResponse fsStatusResponse) {
        if (ObjectUtils.isEmpty(fsStatusResponse) || ObjectUtils.isEmpty(fsStatusResponse.getClients())) {
            return null;
        }
        FileSystem fileSystem = new FileSystem();
        fileSystem.setFsName(fsStatusResponse.getClients().get(0).getFs());
        for (FsStatusResponse.PoolsDTO pool : fsStatusResponse.getPools()) {
            if (FileSystemPoolTypeEnum.METADATA.getMessage().equals(pool.getType())) {
                fileSystem.setMetadataPoolName(pool.getName());
            }
            if (FileSystemPoolTypeEnum.DATA.getMessage().equals(pool.getType())) {
                fileSystem.setDataPoolId(pool.getId());
                fileSystem.setDataPoolName(pool.getName());
            }
        }
        int status = FileSystemStatusEnum.ACTIVE.getStatus();
        for (FsStatusResponse.MdsmapDTO mdsmapDTO : fsStatusResponse.getMdsmap()) {
            if (FileSystemStatusEnum.FAILED.getMessage().equals(mdsmapDTO.getState())) {
                status = FileSystemStatusEnum.FAILED.getStatus();
                break;
            }
        }
        fileSystem.setStatus(status);

        return fileSystem;
    }
}
