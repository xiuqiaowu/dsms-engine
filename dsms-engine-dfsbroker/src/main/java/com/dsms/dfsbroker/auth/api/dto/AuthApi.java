/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.auth.api.dto;

import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.dfsbroker.auth.model.AuthDTO;
import com.dsms.dfsbroker.auth.model.remote.AuthResponse;
import com.dsms.dfsbroker.common.CommandDirector;
import com.dsms.dfsbroker.common.api.CommonApi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
@Slf4j
public class AuthApi {

    public static final int MAX_RETRIES = CommonApi.DEFAULT_RETRY_TIMES;

    @Autowired
    private CommonApi commonApi;

    /**
     * display requested key
     */
    public AuthResponse getAuthKey(RemoteRequest remoteRequest, AuthDTO authDTO) throws Throwable {
        CommandDirector.constructAuthGetKeyRequest(remoteRequest, authDTO);
        return commonApi.remoteCallRequest(remoteRequest, MAX_RETRIES, AuthResponse.class);
    }

}
